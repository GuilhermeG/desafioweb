package stepDefinitions;


import io.cucumber.java.pt.Dado;

import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import pageObjects.CarrinhoPage;
import pageObjects.PaginaIncialPage;
import pageObjects.ResultadodeBuscaPage;



public class stepDefinition {
	
	
	PaginaIncialPage paginaIncialPage = new PaginaIncialPage();
	ResultadodeBuscaPage resultadodeBuscaPage = new ResultadodeBuscaPage();
	CarrinhoPage carrinhoPage = new CarrinhoPage();
	
	@Dado("Que acessei plataforma {string}")
	public void que_acessei_plataforma(String string) {
		paginaIncialPage.login(string);
	}


	@Dado("Realizei a Busca do produto {string}")
	public void realizei_a_Busca_do_produto(String string) {
		paginaIncialPage.buscarProduto(string);
	}


	@Quando("Adicionar o produto ao Carrinho")
	public void adicionar_o_produto_ao_Carrinho() {
		resultadodeBuscaPage.adicionarAoCarrinho();
	}


	@Entao("validar que o produto foi adcionado")
	public void validar_que_o_produto_foi_adcionado() {
		carrinhoPage.validarItemAdicionado();
	}
}

