package pageObjects;

import static webdriver.WebElementUtils.click;
import static webdriver.FluentWaitUtils.*;
import static webdriver.WebElementUtils.getText;

import org.openqa.selenium.By;

public class ResultadodeBuscaPage {
	public static String produto, cor, tamanho;

	public void adicionarAoCarrinho() {
		By rdbTamanho = By.xpath("//a[@qa-automation='product-size']");
		By btnComprar = By.id("buy-button-now");
		By nomeProduto = By.xpath("//h1[@data-productname]");
		String str = getText(nomeProduto);
		produto = str.split(" - ")[0];
		cor = str.split(" - ")[1];
		tamanho = getText(rdbTamanho);
		click(rdbTamanho);
		waitForPageLoaded();
		click(btnComprar);
	}
}
