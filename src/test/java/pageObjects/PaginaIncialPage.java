package pageObjects;

import hooks.Hooks;
import static webdriver.FluentWaitUtils.*;

import static webdriver.SetupDriver.getChromeDriver;
import static webdriver.WebElementUtils.*;

import org.openqa.selenium.By;


public class PaginaIncialPage  {

	public void login(String arg1) {
		driver.get(arg1);
	}

	public void buscarProduto(String string) {
		By txtBusca = By.id("search-input");
		By btnBusca = By.xpath("//button[@title='Buscar']");
		By item = By.xpath("//span[contains(text(),'Sapato Social')]");
		By btnFecharPopUp = By.xpath("//span[@class='fechar-x']");
		waitForPageLoaded();
		if (presenceOfElementLocatedBool(btnFecharPopUp)) {
			click(btnFecharPopUp);
		}
		presenceOfElementLocated(txtBusca);
		sendKeys(txtBusca, string);
		click(btnBusca);
		click(item);

	}

}
