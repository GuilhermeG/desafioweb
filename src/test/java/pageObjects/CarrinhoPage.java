package pageObjects;

import org.openqa.selenium.By;

import org.testng.Assert;

import static webdriver.WebElementUtils.*;

public class CarrinhoPage {

	public void validarItemAdicionado() {
		String produto = getText(By.xpath("//h3[@qa-auto]"));
		String tamanho = getText(By.xpath("//label[contains(text(),'Tamanho:')]/ancestor::p"));
		String cor = getText(By.xpath("//label[contains(text(),'Cor:')]/ancestor::p"));
		Assert.assertEquals(produto.toUpperCase(), ResultadodeBuscaPage.produto);
		Assert.assertEquals(tamanho.split(": ")[1], ResultadodeBuscaPage.tamanho);
		Assert.assertEquals(cor.toUpperCase().split(": ")[1], ResultadodeBuscaPage.cor);
	}

}
