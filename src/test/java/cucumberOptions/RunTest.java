package cucumberOptions;

import io.cucumber.java.After;
import io.cucumber.testng.AbstractTestNGCucumberTests;

import io.cucumber.testng.CucumberOptions;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import java.io.IOException;

@CucumberOptions(
        features = "src/test/java/features",
        glue= {"stepDefinitions","hooks"} )

public class RunTest extends AbstractTestNGCucumberTests {

}
