package hooks;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;

import java.io.IOException;

import static webdriver.SetupDriver.getChromeDriver;
import static webdriver.SetupDriver.quitDriver;

public class Hooks {

	@Before
	public void setupTest(Scenario scenario) {
		getChromeDriver();
	}

	@After
	public void tearDown(Scenario scenario) throws IOException {
		quitDriver();
	}

}
