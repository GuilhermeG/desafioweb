package webdriver;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;

public class SetupDriver {

    public static WebDriver driver;
    private static ChromeOptions chromeOptions = new ChromeOptions();

    public static WebDriver getChromeDriver() {
        System.setProperty(ChromeDriverService.CHROME_DRIVER_SILENT_OUTPUT_PROPERTY,"true");
        chromedriver().setup();
        chromeOptions.addArguments("start-maximized");
        chromeOptions.addArguments("enable-automation");
        chromeOptions.addArguments("no-sandbox");
        driver = new ChromeDriver(chromeOptions);
        return driver;
    }

    public static void quitDriver(){
        driver.quit();
    }


}
