package webdriver;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import Utils.Constats;

import java.time.Duration;
import java.util.List;

import static java.time.Duration.ofSeconds;

public class FluentWaitUtils extends SetupDriver {

    public static WebElement visibilityOfElementLocated(By element) {
        return new FluentWait<>(driver)
                .withTimeout(ofSeconds(Constats.timeout))
                .pollingEvery(Duration.ofMillis(250))
                .ignoring(Exception.class)
                .until(ExpectedConditions.visibilityOfElementLocated(element));
    }

    public static WebElement elementToBeClickable(By element) {
        return new FluentWait<>(driver)
                .withTimeout(ofSeconds(Constats.timeout))
                .pollingEvery(Duration.ofMillis(250))
                .ignoring(Exception.class)
                .until(ExpectedConditions.elementToBeClickable(element));
    }

    public static WebElement presenceOfElementLocated(By element) {
        return new FluentWait<>(driver)
                .withTimeout(ofSeconds(Constats.timeout))
                .pollingEvery(Duration.ofMillis(250))
                .ignoring(Exception.class)
                .until(ExpectedConditions.presenceOfElementLocated(element));
    }
    
    public static boolean presenceOfElementLocatedBool(By element) {
        WebElement element2 = new FluentWait<>(driver)
                .withTimeout(ofSeconds(Constats.timeout))
                .pollingEvery(Duration.ofMillis(250))
                .ignoring(Exception.class, NoSuchElementException.class)
                .until(ExpectedConditions.presenceOfElementLocated(element));
        if (element2.isDisplayed()) 
        	return true;
        return false;
        
    }

    public static void waitForPageLoaded() {

        ExpectedCondition<Boolean> expectation = driver1 -> {
            assert driver1 != null;
            return ((JavascriptExecutor) driver1).executeScript(
                    "return document.readyState").toString().equals("complete");
        };
        try {
            Thread.sleep(1000);
            WebDriverWait wait = new WebDriverWait(driver, Constats.timeoutSiteLoad);
            wait.until(expectation);
        } catch (Throwable error) {
            Assert.fail("Timeout waiting for Page Load Request to complete." + error);
        }
    }
}
