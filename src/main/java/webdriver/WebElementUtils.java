package webdriver;

import org.openqa.selenium.*;

import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.*;

import java.util.List;

import static webdriver.FluentWaitUtils.*;
import static java.time.Duration.ofSeconds;

public class WebElementUtils extends SetupDriver {

	public static void click(By element) {
		elementToBeClickable(element).click();
	}

	public static String getText(By element) {
		return presenceOfElementLocated(element).getText();
	}

	public static void sendKeys(By element, String text) {
		elementToBeClickable(element).sendKeys(text);
	}

}
